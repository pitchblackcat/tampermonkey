// ==UserScript==
// @name         Ctrl + s For Phabricator
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Just hit ctrl + s to post your current comments
// @author       Bas
// @match        https://phabricator.expert-shops.com/*
// @grant        none
// @require https://code.jquery.com/jquery-2.1.4.min.js
// ==/UserScript==

var submitSelector = '[name="__submit__"]';

(function() {
    'use strict';

    $(document).on('keydown', function(e){
        if(e.ctrlKey && e.which === 83){ // Check for the Ctrl key being pressed, and if the key = [S] (83)

            $(submitSelector).trigger("click");

            e.preventDefault();
            return false;
        }
    });
})();