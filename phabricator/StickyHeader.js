// ==UserScript==
// @name         Follow Menu
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try to censor Jenkins
// @author       Bas
// @match        https://phabricator.expert-shops.com/*
// @grant        none
// @require https://code.jquery.com/jquery-2.1.4.min.js
// ==/UserScript==
/* jshint -W097 */
'use strict';

var body = ".phabricator-standard-page-body";
var mainMenu = ".phabricator-main-menu";

$(mainMenu)
    .css("position", "fixed")
    .css("width", "100%")

$(body)
    .css("padding-top", $(mainMenu).height());
