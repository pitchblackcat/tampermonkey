// ==UserScript==
// @name         Censor Jenkins
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try to censor Jenkins
// @author       Bas
// @match        https://phabricator.expert-shops.com/D*
// @grant        none
// @require https://code.jquery.com/jquery-2.1.4.min.js
// ==/UserScript==
/* jshint -W097 */
'use strict';

var jenkinsLink = ".phui-handle.phui-link-person[href='/p/jenkins-ci/']";
var commentClass = ".phui-timeline-inner-content";
var titleClass = ".phui-timeline-title";
var coreClass = ".phui-timeline-core-content";

init();
$(".phui-timeline-view").bind("DOMSubtreeModified",function(){
    init();
});

function init() {
    $(jenkinsLink).not(":last").each(function() {
        var $comment = $(this).parents(commentClass);
        hideComment($comment);
    });
}

function hideComment($comment) {
    $comment.find(coreClass).slideUp()
    makeCommentTogglable($comment);
}

function makeCommentTogglable($comment) {
    var $core = $comment.find(coreClass);
    $comment
        .find(titleClass)
        .unbind("click")
        .click(function() {
            $core.slideToggle("fast");
        });
}

